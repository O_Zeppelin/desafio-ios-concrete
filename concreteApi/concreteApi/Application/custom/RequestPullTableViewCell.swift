//
//  RequestPullTableViewCell.swift
//  concreteApi
//
//  Created by Anderson Silva on 08/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

class RequestPullTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var dtPull: UILabel!
    
    weak var baseInterface : BaseViewInterface?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(request:RequestsBaseClass?) {
        
        guard request != nil else {
            self.baseInterface?.showAlertModal(title: "Ops!", msg: "Houve um error. Tene novamente.", btn: "OK")
            return
        }
        
        self.imagem.kf.setImage(with: URL(string: (request?.user?.avatarUrl)!), placeholder: UIImage(named: "avatar"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        self.imagem.asCircle()
        
        self.titulo.text = request?.title
        self.body.text   = request?.body
        self.dtPull.text = request?.createdAt?.formataData
        self.name.text = request?.user?.login
        
    }
    
}
