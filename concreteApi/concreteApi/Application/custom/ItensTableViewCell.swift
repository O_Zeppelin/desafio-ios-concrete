//
//  ItensTableViewCell.swift
//  concreteApi
//
//  Created by Anderson Silva on 07/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

class ItensTableViewCell: UITableViewCell {

    @IBOutlet weak var nomeRepositorio: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var descricao: UILabel!
    @IBOutlet weak var forks: UILabel!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var nome: UILabel!
    
    weak var baseInterface : BaseViewInterface?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(item:ItemItems?) {
        
        guard item != nil else {
            self.baseInterface?.showAlertModal(title: "Ops!", msg: "Houve um error. Tene novamente.", btn: "OK")
            return
        }
        
        self.imgAvatar.kf.setImage(with: URL(string: (item?.owner?.avatarUrl)!), placeholder: UIImage(named: "avatar"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        self.imgAvatar.asCircle()
        
        self.nomeRepositorio.text = item?.name
        self.descricao.text = item?.descriptionValue
        self.forks.text = String(describing: item!.forks!)
        self.stars.text = String(describing: item!.stargazersCount!)
        self.userName.text = item?.owner?.login
        self.nome.text = item?.fullName
        
    }
    
}
