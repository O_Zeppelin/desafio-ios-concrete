//
//  RequestsLinks.swift
//
//  Created by Anderson Silva on 08/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RequestsLinks: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let reviewComments = "review_comments"
    static let statuses = "statuses"
    static let issue = "issue"
    static let commits = "commits"
    static let selfs = "self"
    static let comments = "comments"
    static let html = "html"
    static let reviewComment = "review_comment"
  }

  // MARK: Properties
  public var reviewComments: RequestsReviewComments?
  public var statuses: RequestsStatuses?
  public var issue: RequestsIssue?
  public var commits: RequestsCommits?
  public var selfs: RequestsSelf?
  public var comments: RequestsComments?
  public var html: RequestsHtml?
  public var reviewComment: RequestsReviewComment?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    reviewComments <- map[SerializationKeys.reviewComments]
    statuses <- map[SerializationKeys.statuses]
    issue <- map[SerializationKeys.issue]
    commits <- map[SerializationKeys.commits]
    selfs <- map[SerializationKeys.selfs]
    comments <- map[SerializationKeys.comments]
    html <- map[SerializationKeys.html]
    reviewComment <- map[SerializationKeys.reviewComment]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = reviewComments { dictionary[SerializationKeys.reviewComments] = value.dictionaryRepresentation() }
    if let value = statuses { dictionary[SerializationKeys.statuses] = value.dictionaryRepresentation() }
    if let value = issue { dictionary[SerializationKeys.issue] = value.dictionaryRepresentation() }
    if let value = commits { dictionary[SerializationKeys.commits] = value.dictionaryRepresentation() }
    if let value = selfs { dictionary[SerializationKeys.selfs] = value.dictionaryRepresentation() }
    if let value = comments { dictionary[SerializationKeys.comments] = value.dictionaryRepresentation() }
    if let value = html { dictionary[SerializationKeys.html] = value.dictionaryRepresentation() }
    if let value = reviewComment { dictionary[SerializationKeys.reviewComment] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.reviewComments = aDecoder.decodeObject(forKey: SerializationKeys.reviewComments) as? RequestsReviewComments
    self.statuses = aDecoder.decodeObject(forKey: SerializationKeys.statuses) as? RequestsStatuses
    self.issue = aDecoder.decodeObject(forKey: SerializationKeys.issue) as? RequestsIssue
    self.commits = aDecoder.decodeObject(forKey: SerializationKeys.commits) as? RequestsCommits
    self.selfs = aDecoder.decodeObject(forKey: SerializationKeys.selfs) as? RequestsSelf
    self.comments = aDecoder.decodeObject(forKey: SerializationKeys.comments) as? RequestsComments
    self.html = aDecoder.decodeObject(forKey: SerializationKeys.html) as? RequestsHtml
    self.reviewComment = aDecoder.decodeObject(forKey: SerializationKeys.reviewComment) as? RequestsReviewComment
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(reviewComments, forKey: SerializationKeys.reviewComments)
    aCoder.encode(statuses, forKey: SerializationKeys.statuses)
    aCoder.encode(issue, forKey: SerializationKeys.issue)
    aCoder.encode(commits, forKey: SerializationKeys.commits)
    aCoder.encode(selfs, forKey: SerializationKeys.selfs)
    aCoder.encode(comments, forKey: SerializationKeys.comments)
    aCoder.encode(html, forKey: SerializationKeys.html)
    aCoder.encode(reviewComment, forKey: SerializationKeys.reviewComment)
  }

}
