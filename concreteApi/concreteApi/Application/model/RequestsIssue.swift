//
//  RequestsIssue.swift
//
//  Created by Anderson Silva on 08/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RequestsIssue: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let href = "href"
  }

  // MARK: Properties
  public var href: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    href <- map[SerializationKeys.href]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = href { dictionary[SerializationKeys.href] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.href = aDecoder.decodeObject(forKey: SerializationKeys.href) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(href, forKey: SerializationKeys.href)
  }

}
