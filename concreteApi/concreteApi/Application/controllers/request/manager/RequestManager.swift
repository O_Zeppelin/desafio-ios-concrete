//
//  RequestManager.swift
//  concreteApi
//
//  Created by Anderson Silva on 08/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

protocol RequestInterface : class {
    func updateRequest(request:[RequestsBaseClass])
}

class RequestManager : NSObject {
    
    private lazy var business = {
        return RequestBusiness.init()
    }()
    
    weak var requestInterface : RequestInterface?
    weak var baseInterface : BaseViewInterface?
    
    func getRequestPull(itens:ItemItems?) {
        
        self.baseInterface?.loadingShow()
        
        guard itens != nil else {
            self.baseInterface?.showAlertModal(title: "Ops!", msg: "Algo deu errado! Tente novamente.", btn: "OK")
            return
        }
        
        
        business.getRequests(fullName: (itens?.fullName)!) { (resultado, error) in
            
            self.baseInterface?.loadingHide()
            
            guard resultado != nil else {
                self.baseInterface?.showAlertModal(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                return
            }
            
            self.requestInterface?.updateRequest(request: resultado!)
            
        }
        
    }
    
    func openUrlSafari(url:String?) {
        
        guard url != nil || !(url?.isEmpty)! else {
            self.baseInterface?.showAlertModal(title: "Ops!", msg: "Algo deu errado! Tente novamente.", btn: "OK")
            return
        }
        
        self.baseInterface?.openURLSafari(url: url!)
        
    }
    
}
