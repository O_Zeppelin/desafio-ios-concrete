//
//  ItemProvider.swift
//  concreteApi
//
//  Created by Anderson Silva on 07/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class ItemProvider : NSObject {
    
    func getItens(page:Int,completion:@escaping(_ itens:ItemBaseClass?, _ error:Error?) -> Void) -> Void {
        
        request(Constants.URL_ITEM + String(describing: page)).responseObject { (response:DataResponse<ItemBaseClass>) in
            guard let item = response.result.value else {
                completion(nil, response.result.error)
                return
            }
            completion(item, nil)
        }
    }
    
}
