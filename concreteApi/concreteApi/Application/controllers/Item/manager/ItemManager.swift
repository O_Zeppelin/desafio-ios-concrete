//
//  ItemManager.swift
//  concreteApi
//
//  Created by Anderson Silva on 07/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

protocol ItemInterface : class {
    func updateItem(totalCount:Int, items:[ItemItems])
}

class ItemManager : NSObject {
    
    private lazy var business = {
        return ItemBusiness.init()
    }()
    
    private var page = 1
    var objItens = [ItemItems]()
    
    weak var itemInterface : ItemInterface?
    weak var baseInterface : BaseViewInterface?
    
    func getItens() {
        
        self.baseInterface?.loadingShow()

        business.getItens(page: self.page) { (itemBaseClass, error) in
            
            self.baseInterface?.loadingHide()
            
            guard itemBaseClass != nil else {
                self.baseInterface?.showAlertModal(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                return
            }
            
            if((itemBaseClass?.items)!.count > 0 ){
                
                self.page = self.page+1
                
                for myItem in (itemBaseClass?.items)!{
                    self.objItens.append(myItem)
                }
                
                self.itemInterface?.updateItem(totalCount: (itemBaseClass?.totalCount)!, items: self.objItens)

            }
            
        }
        
    }
 
}
